<!DOCTYPE html>
<html>
<head>
	<title>Polyglot</title>
	<meta charset="utf-8">
	<!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script type="text/javascript" src="js/lib.js"></script>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="background-color: #f5f5f5">
	<nav>
		<div class="nav-wrapper" style="background: #8A0829">
			<div class="container">
				<a href="index.php" class="brand-logo">Polyglot</a>
				<ul class="right hide-on-med-and-down">
					<li><a href="index.php">Главная</a></li>
					<li><a href="#" >Статистика</a></li>
					<li><a href="#" >Словарь</a></li>
					<li><a onclick="exit()">Выйти</a></li>
				</ul>
			</div>
		</div>
	</nav> 
	<br><br>

	<?
	if ($_GET['сourse'] && $_GET['spec']){?> 
		<div class="row">
		<div class="col s8 offset-s2">
		<div class="card grey light-blue lighten-5">
			<div class="style" style="padding: 20px" id="container">

			<a style="font-size: 28px; color: black; font-weight: 399;">Навигация по тестам</a><br><br>					
			<a style="font-size: 18px; color: black; font-weight: 399;"> 
				<span style="font-weight: 400;">Курс: </span><? echo $_GET['сourse']; ?> 
				<span style="font-weight: 400;">Cпециальность: </span><? echo $_GET['name']; ?></a>						
			<div class="divider"></div><br>
			<ul class="collapsible" data-collapsible="accordion" id="ulSpec">					    
		  	</ul>
			<script>						
				var spec = '<? echo $_GET['spec'] ?>';
				var course = '<? echo $_GET['сourse'] ?>';

				$.ajax({
					type: 'POST',
					url: 'API.php',
					dataType: 'text',
					data: {'SelectTopic': true, 'spec': spec, 'course': course},
					success: function(data){
						data = JSON.parse(data);
	
						for (var i = 0; i < data.length; i++){
							var ul = document.querySelector('#ulSpec');									
							var li = document.createElement('li'),
								header = document.createElement('div'),
								a = document.createElement('a'),
								icon = document.createElement('i');

							a.style = "color: black; font-size: 18px";
							icon.className = "material-icons";
							icon.innerHTML = "chevron_right";
							header.className = "collapsible-header";
							a.innerHTML = data[i].topic;
							a.href = "?test=" + data[i].id;
							header.appendChild(icon);
							header.appendChild(a);
							li.appendChild(header);
							ul.appendChild(li);
						}
					}
				})						
			</script>
			<?	}
	else if ($_GET['test']){
			$host = "localhost";
			$userDB = "root";
			$passwordDB = "";
			mysql_connect($host, $userDB, $passwordDB);
			mysql_select_db("Polyglot");
			$idTest = $_GET['test'];
			$topic = mysql_query("SELECT * FROM `Topics` WHERE `id`=$idTest");
			$topic = mysql_fetch_assoc($topic);
			mysql_close();
		?>

		<div id="modal1" class="modal">
		    <div class="modal-content">
		      <h4>Вы набрали <span id="points" style="color: red"></span><span id="word"></span></h4>
		    </div>
		    <div class="modal-footer">
		      <a class="modal-action modal-close waves-effect waves-green btn-flat" onclick="location.reload()">Пройти еще раз</a>
		      <a href="index.php" class="modal-action modal-close waves-effect waves-green btn-flat">Завершить</a>
		    </div>
  		</div>

		<div class="row">
		<div class="col s6 offset-s3">
		<div class="card grey light-blue lighten-5">
		<div class="style" id="container">
			<div class="startTestBox">
				<a style="font-size: 28px; color: black; font-weight: 399;">Тест: <? echo $topic['topic'] ?></a>
				<div class="divider"></div><br>
				<div class="row">
					<div class="col">
						<span style="font-size: 16px">Каждая попытка прохождения теста - записывается в вашу статистику и доступна для просмотра администраторам данного сервиса. Советуем не шалить с тестами:D</span>
					</div>
				</div>
				<div class="row">
					<div class="col s6 offset-s4" >
						<a class="waves-effect waves-light  btn" id="startTest" style="background-color: #8A0829">Пройти тест</a>
					</div>
				</div>
			</div>
			<div class="testBox" style="display: none">
				<span style="font-size: 28px; color: black; font-weight: 399;">Выберите правильный ответ:</span>
				<div class="divider"></div><br>
			
				<div id="test">
				<ul>
				<li v-for="(iWord, i) in words">
					<div class="row">	
					<p style="color: black; font-size: 20px">№{{ i+1 }}</p>		
					<div class="col s8">
					<span style="font-size: 18px; ">Слово: <a style="font-size: 20px">{{ iWord.word }}</a></span><br>
					<span style="font-size: 18px;">Пример использования: <a>{{ iWord.sentence }}</a></span>
					</div>
					<div class="col s3 offset-s1">
 					<form>
 						<p>
 							<input type="radio" v-bind:id="i + iWord.test[0]" v-on:click="iWord.answer = iWord.test[0]" name="group">
 							<label v-bind:for="i + iWord.test[0]">{{ iWord.test[0] }}</label>
 						</p>
 						<p>
 							<input type="radio" v-bind:id="i + iWord.test[1]" v-on:click="iWord.answer = iWord.test[1]" name="group">
 							<label v-bind:for="i + iWord.test[1]">{{ iWord.test[1] }}</label>
 						</p>
  						<p>
 							<input type="radio" v-bind:id="i + iWord.test[2]" v-on:click="iWord.answer = iWord.test[2]" name="group">
 							<label v-bind:for="i + iWord.test[2]">{{ iWord.test[2] }}</label>
 						</p> 
 					</form><br>
   					</div>
   					<hr>
   					</div>	
   				</li>
   				<div class="row">
   				<div class="col s5 offset-s4">
   					<a class="waves-effect waves-light btn modal-trigger" onclick="checkTest()" href="#modal1" style="background-color: #8A0829">Завершить тест</a>
   				</div>
   				</div>
				</ul>
				</div>			
			</div>
			<script>
				var idTopic = <? echo $_GET['test'] ?>;
				var numberTest = 0;	
				var wordTest = [];			
				$.ajax({
					type: 'POST',
					url: 'API.php',
					dataType: 'text',
					data: {'getWord': true, 'topic': idTopic},
					success: function(data){						
						document.querySelector('#startTest').addEventListener('click', function (){
							document.querySelector('.startTestBox').style = "display: none";
							document.querySelector('.testBox').style = "display: block";
							data = JSON.parse(data);
							wordTest = data;
							console.log(data);
							var tests = [];
							for (var i = 0; i < data.length; i++){
								var Word, buffTest = [];
								Word = JSON.stringify(data);
								Word = JSON.parse(Word);

								// ДОБАВЛЕНИЕ ЭЛЕМЕНТОВ В МАССИВ
								buffTest.push(Word[i].translate);
								Word.splice(i,1);

								var randNumber = rnd(0, Word.length);
								buffTest.push(Word[randNumber].translate);
								Word.splice(randNumber,1);

								randNumber = rnd(0, Word.length);
								buffTest.push(Word[randNumber].translate);
								Word.splice(randNumber,1);

								// ПЕРЕМЕШИВАНИЕ
								buffTest = shuffle(buffTest);
								data[i].test = buffTest;
							}
							var app = new Vue({
								el: '#test',
								data: {
									words: data
								} 
							})
						});	
					}
				});	
				
			</script>

	<?	}
	else{?>
		<div class="row">
		<div class="col s10 offset-s1">
		<div class="card grey light-blue lighten-5">
		<div class="style" style="padding: 20px" id="container">
			<a style="font-size: 28px; color: black; font-weight: 399;">Навигация по тестам</a>
			<div class="divider"></div><br>						
			<ul class="collapsible popout" data-collapsible="accordion" id="ulSelect">					    
		  	</ul>
			
			<script type="text/javascript">

				$(document).ready(function(){
				    $('.collapsible').collapsible();
				});       
				$.ajax({
					type: 'POST',
					url: 'API.php',
					dataType: 'text',
					data: {'getSection': true},
					success: function(data){
						var arrSpec = JSON.parse(data);
						arrCourse = ['I  Первый','II Второй','III Третий','IV Четвертый'];

						for (var i = 0; i < arrSpec.length; i++){
							var ul = document.querySelector('#ulSelect');
							var li = document.createElement('li'),
							header = document.createElement('div'),
							body = document.createElement('div'),
							logo = document.createElement('i'),
							name = document.createElement('span');

							header.className = "collapsible-header";
							logo.className = "material-icons";
							logo.innerHTML = arrSpec[i].icon;
							name.innerHTML = arrSpec[i].name;
							name.style="color: black; font-size: 20px; font-weight: 399;"
							header.appendChild(logo);
							header.appendChild(name);
							
							body.className = "collapsible-body";
							body.style = "font-size: 16px;"
							for (var j = 0; j < arrCourse.length; j++){
								var a = document.createElement('a');
								a.innerHTML = arrCourse[j];
								a.style = "color: black;";
								a.href = document.location.href + '?сourse=' + (1+j) + '&spec=' + arrSpec[i].id + '&name=' + arrSpec[i].name;
								body.appendChild(a);
								body.appendChild(document.createElement('br'));
							}	
							li.appendChild(header);
							li.appendChild(body);
							ul.appendChild(li);
						}
					}
				}) 						
			</script>
			 <? } ?>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
   		$('.modal').modal();
 	});
	function checkTest(){
		var points = 0;
		for (var i = 0; i < wordTest.length; i++) {
			if (wordTest[i].answer == wordTest[i].translate) points++
		}
		if (points < 2 && points > 0) document.querySelector('#word').innerHTML = " балл";
		else if (points > 1 && points < 5) document.querySelector('#word').innerHTML = " балла";
			else document.querySelector('#word').innerHTML = " баллов";
		document.querySelector('#points').innerHTML = points;
		document.location = "#modal1";
		
		$.ajax({
			type: 'POST',
			url: 'API.php',
			dataType: 'text',
			data: {'addResult': true, 'points': points, 'topic': idTopic}
		})
		
	}
</script>
<div class="footer-copyright">
	<br>
    <div class="center-align">
    	<h6>© by Nilcon</h6>
    </div>
</div>
</body>
</html>