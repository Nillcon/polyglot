<!DOCTYPE html>
<html>
<head>
	<title>Polyglot</title>
	<meta charset="utf-8">
	<!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="background-color: #f5f5f5">
<nav>
	<div class="nav-wrapper" style="background: #8A0829">
		<div class="container">
			<a href="index.php" class="brand-logo">Polyglot</a>
			<ul class="right hide-on-med-and-down">
				<li><a href="index.php">Главная</a></li>
				<li><a href="#" >Статистика</a></li>
				<li><a href="#" >Словарь</a></li>
				<li><a href="#">Выйти</a></li>
			</ul>
		</div>
	</div>
</nav> 
	<br><br>


  <!-- Модульное окно добавления полей -->
  <div id="modal1" class="modal">
    <div class="modal-content">
      <h5>Сколько строк вы хотите добавить?</h5>
      <input type="text" id="addLines">
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Отменить</a>
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat" onclick="addLines()">Добавить</a>
    </div>
  </div>

	<div class="row">
	<div class="col s10 offset-s1">
	<div class="card grey light-blue lighten-5">
	<div class="style" style="padding: 20px" id="container">
	<?php

	$host = "localhost";
	$userDB = "root"; 
	$passwordDB = "";

		if ( $_GET['page']){
			$connect = mysql_connect($host, $userDB, $passwordDB);
			$DB = mysql_select_db("Polyglot");
			if (!$connect || !$DB) mysql_error();

			$login = $_COOKIE['login'];
			$page = $_GET['page'];

			$queryUsers = mysql_query("SELECT * FROM `Users` WHERE `login`='$login'");
			$queryWord = mysql_query("SELECT * FROM `Words` WHERE `idTopics`= $page");
			$queryTopic = mysql_query("SELECT * FROM `Topics` WHERE `id`=$page");			
			$dataUser = mysql_fetch_assoc($queryUsers);
			mysql_close();

			$topic = mysql_fetch_assoc($queryTopic);
			?> <h5><? echo $topic['topic'] ?> </h5><br>

			<div class="divider"></div>
			<div id="app">

			 <?	
			if ($dataUser['isAdmin'] == 'false'){
				$number = 1;
				?>					
					<table class="striped centered" id="table">
        		<thead>
         			<tr id="tableHeader">
         			<th>№</th>
              		<th>Слово</th>
              		<th>Перевод</th>
              		<th>Использование</th>
          			</tr>
        		</thead>
        		<tbody>
				<?
				while ($arr = mysql_fetch_assoc($queryWord)){ ?>
					<tr>
						<td><? echo $number++ ?> </td>
		            	<td><? echo $arr['word'] ?></td>
		            	<td><? echo $arr['translate'] ?></td>
		            	<td><? echo $arr['sentence'] ?></td>
		          	</tr>
			<? }; ?>
				</tbody>
				</table>
			<?
			}
			else if ($dataUser['isAdmin'] == 'true'){
				?> 
				<table class="striped centered" id="table" style="padding-bottom: 30px">
        		<thead>
         			<tr id="tableHeader">
         			<th>№</th>
              		<th>Слово</th>
              		<th>Перевод</th>
              		<th>Использование</th>
              		<th></th>
          			</tr>
        		</thead>
        		<tbody>	
				<div class="fixed-action-btn">
		 			<a class="btn-floating btn-large" style="background-color: #8A0829">
		    			<i class="large material-icons">mode_edit</i>
  					</a>
			  		<ul>
					    <li><a class="btn-floating red" onclick="save();"><i class="material-icons">save</i> </a></li>
					    <li><a class="btn-floating yellow darken-1 btn modal-trigger" href="#modal1"><i class="material-icons">add</i></a></li>
					    <li><a class="btn-floating green"><i class="material-icons">publish</i></a></li>
					    <li><a class="btn-floating blue"><i class="material-icons">attach_file</i></a></li>
			  		</ul>
				</div>
					<tr v-for="(words, i) in word" v-show="words.word">
						<td>{{ i + 1 }}</td>
		            	<td><input type="text" v-model="words.word"></td>
		            	<td><input type="text" v-model="words.translate"></td>
		            	<td><input type="text" v-model="words.sentence"></td>
		            	<td><a href="#" style="color: black" v-on:click="del(i)"><i class="large material-icons" style="font-size: 20px">clear</i></a></td>
		          	</tr>
		        </tbody>
				</table>
				
				<script>						
					var answer,
						topic = <? echo $_GET['page'] ?>;
				
					$.ajax({
						type: 'POST',
						url: 'API.php',
						dataType: 'text',
						data: {'getWord': true, 'topic': topic},
						success: function(data){
							answer = JSON.parse(data);
							var app = new Vue({
								el: '#app',
								data:{
									word: answer
								}									
							});
						}
					});	
				</script>
					
				<?
			};
		}
		else {

		?>
			<a style="font-size: 28px; color: black; font-weight: 399;">Словарь</a> 
			<div class="divider"></div><br>
			
			<ul class="collapsible" id="selection" style="font-size: 23px; width: 100%;margin: auto; font-weight: 399;">
		<? 
			$connect = mysql_connect($host, $userDB, $passwordDB);
			$DB = mysql_select_db("Polyglot");
			if (!$connect || !$DB) mysql_error();

			$querySection = mysql_query("SELECT * FROM `Section`");
			while ($Section = mysql_fetch_assoc($querySection)){?>
				
				<li> 
					<div class="collapsible-header">
						<i class="material-icons"><? echo $Section['icon'] ?></i>
						<a style="color: black; font-size: 20px"><? echo $Section['name'] ?></a>
					</div>
					<div class="collapsible-body" style="padding-left: 60px;"> 
						<?	
							$id = $Section['id'];
							$queryTopic = mysql_query("SELECT * FROM `Topics` WHERE `idSpecialty`= '$id'");

							while ($Topics = mysql_fetch_assoc($queryTopic)){ ?>	
													
								<i class="material-icons" style="font-size: 13px;">chevron_right</i>
								<a href="<? echo $Topics['link'] ?>" style="color: black; font-size: 20px;"><? echo $Topics['topic'] ?></a>										
								<div class="divider"></div>
							
							<?}
						?>
					</div>
				</li>

			<?}?>
			</ul>
		<? }; ?>	
</div>
</div>
</div>
</div>
</div>
<div class="footer-copyright">
	<br>
    <div class="center-align">
    	<h6>© by Nilcon</h6>
    </div>
</div>

    <script type="text/javascript">
    var topic;
	$(document).ready(function(){
		$('.modal').modal();
	});

	function save() {
		var saveWord = JSON.stringify(answer);
		$.ajax({
			type: 'POST',
			url: 'API.php',
			dataType: 'text',
			data: {'save': true, 'arrayWord': saveWord},
			success: function(data){
				Materialize.toast('Изменения сохранены', 3000, 'rounded');
				console.log(data);
			}
		})
	};

	function add(){
		obj = {
			id : 'null',
			word: ' ',
			translate: '',
			sentence: '',
			idTopics: topic
		}
		answer.push(obj);
	}

	function del(id){
		answer[id].word = '';
		Materialize.toast('Запись удалена', 3000, 'rounded');
	}

	function addLines(){
		var lines = document.querySelector('#addLines');
		if (!isNaN(lines)) add();
		else if (isNaN(lines)){
			for (var i = 0; i < +lines.value; i++){
				add();
			} 
		}
	}
    </script>
</body>
</html>