<!DOCTYPE html>
<html>
<head>
	<title>Polyglot</title>
	<meta charset="utf-8">
	<!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="background-color: #f5f5f5">

	<div class="fixed-action-btn">
 		<a class="btn-floating btn-large" style="background-color: #8A0829">
    		<i class="large material-icons">mode_edit</i>
  		</a>
  		<ul>
		    <li><a class="btn-floating red" onclick="save()"><i class="material-icons">save</i></a></li>
		    <li><a class="btn-floating yellow darken-1"><i class="material-icons">format_quote</i></a></li>
		    <li><a class="btn-floating green"><i class="material-icons">publish</i></a></li>
		    <li><a class="btn-floating blue"><i class="material-icons">attach_file</i></a></li>
  		</ul>
	</div>

	<nav>
		<div class="nav-wrapper" style="background: #8A0829">
			<div class="container">
				<a href="index.php" class="brand-logo">Polyglot</a>
				<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
				<ul class="right hide-on-med-and-down">
					<li><a href="index.php" >Главная</a></li>
					<li><a href="#" >Статистика</a></li>
					<li><a href="#" >Словарь</a></li>
					<li><a href="#">Выйти</a></li>
				</ul>
				<ul class="side-nav" id="mobile-demo">
					<li><a href="index.php">Главная</a></li>
					<li><a href="#">Статистика</a></li>
					<li><a href="#">Словарь</a></li>
					<li><a href="authorization.php">Авторизоваться</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<br><br>
	<div class="row">
	<div class="col s8 offset-s2">
	<div class="card grey light-blue lighten-5">
	<div class="style" style="padding: 20px" id="container">

	<?php

	$host = "localhost";
	$userDB = "root"; 
	$passwordDB = "";

		if ( $_GET['page']){
			$connect = mysql_connect($host, $userDB, $passwordDB);
			$DB = mysql_select_db("Polyglot");
			if (!$connect || !$DB) mysql_error();

			$login = $_COOKIE['login'];
			$page = $_GET['page'];

			$queryUsers = mysql_query("SELECT * FROM `Users` WHERE `login`='$login'");
			$queryWord = mysql_query("SELECT * FROM `Words` WHERE `idTopics`= $page");
			$queryTopic = mysql_query("SELECT * FROM `Topics` WHERE `id`=$page");			
			$dataUser = mysql_fetch_assoc($queryUsers);
			mysql_close();

			$topic = mysql_fetch_assoc($queryTopic);
			?> <h5><? echo $topic['topic'] ?> </h5><br>

			<div class="divider"></div>
			<table class="striped centered">
        		<thead>
         			<tr>
         			<th>№</th>
              		<th>Слово</th>
              		<th>Перевод</th>
              		<th>Использование</th>
          			</tr>
        		</thead>
        		<tbody>

			 <?	
			if ($dataUser['isAdmin'] == 'false'){
				$number = 1;
				while ($arr = mysql_fetch_assoc($queryWord)){ ?>
					<tr>
						<td><? echo $number++ ?> </td>
		            	<td><? echo $arr['word'] ?></td>
		            	<td><? echo $arr['translate'] ?></td>
		            	<td><? echo $arr['sentence'] ?></td>
		          	</tr>
			<? 
				};
			}
			else if ($dataUser['isAdmin'] == 'true'){
				
			};
		}
		else { ?>
			<a style="font-size: 28px; color: black; font-weight: 399;">Словарь</a> 
			<div class="divider"></div><br>
			<ul class="collapsible" id="selection" style="font-size: 23px; width: 100%;margin: auto; font-weight: 399;">
		<?
			$connect = mysql_connect($host, $userDB, $passwordDB);
			$DB = mysql_select_db("Polyglot");
			if (!$connect || !$DB) mysql_error();

			$querySection = mysql_query("SELECT * FROM `Section`");
			while ($Section = mysql_fetch_assoc($querySection)){?>
				
				<li> 
					<div class="collapsible-header">
						<i class="material-icons"><? echo $Section['icon'] ?></i>
						<a style="color: black; font-size: 20px"><? echo $Section['name'] ?></a>
					</div>
					<div class="collapsible-body" style="padding-left: 60px; padding-right: 60px; display: block;"> 
						<?	
							$id = $Section['id'];
							$queryTopic = mysql_query("SELECT * FROM `Topics` WHERE `idSpecialty`= '$id'");

							while ($Topics = mysql_fetch_assoc($queryTopic)){ ?>								
								<i class="material-icons" style="font-size: 13px;">chevron_right</i>
								<a href="<? echo $Topics['link'] ?>" style="color: black; font-size: 20px;"><? echo $Topics['topic'] ?></a>
								<div class="divider"></div>
							<?}
						?>
					</div>
				</li>

			<?}
		};
		?>
</tbody>
</table>
</div>
</div>
</div>
</div>
<div class="footer-copyright">
	<br>
    <div class="center-align">
    	<h6>© by Nilcon</h6>
    </div>
</div>
    <script type="text/javascript">
    var topic;
    	$(document).ready(function(){
    		$(".button-collapse").sideNav();

		});
		function save(){
			var words = {
				'word': null,
				'translate': null,
				'sentence': null
			}
			var lol = document.querySelector('.word');
			console.log(lol);
		}
    </script>
</body>
</html>