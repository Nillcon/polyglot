<!DOCTYPE html>
<html>
<head>
	<title>Polyglot</title>
	<meta charset="utf-8">
	<!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="background-color: #f5f5f5">

	<ul id="nav-user" class="dropdown-content">
		<li><a href="#!">О проекте</a></li>
		<li class="divider"></li>
  		<li><a href="#!">Настройки</a></li>
  		<li class="divider"></li>
  		<li><a href="#!">Выход</a></li>
	</ul>

	<nav>
		<div class="nav-wrapper" style="background: #8A0829">
			<div class="container">
				<a href="index.php" class="brand-logo">Polyglot</a>
				<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
				<ul class="right hide-on-med-and-down">
					<li><a href="index.php" >Главная</a></li>
					<li><a href="#" >Статистика</a></li>
					<li><a href="#" >Словарь</a></li>
					<li><a href="#">Выйти</a></li>
				</ul>
				<ul class="side-nav" id="mobile-demo">
					<li><a href="index.php">Главная</a></li>
					<li><a href="#">Статистика</a></li>
					<li><a href="#">Словарь</a></li>
					<li><a href="authorization.php">Авторизоваться</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<br><br>
	<div class="row">
	<div class="col s6 offset-s3">
	<div class="card grey light-blue lighten-5">
	<div class="style" style="padding: 20px" id="container">

	<?php 
	$host = "localhost";
	$userDB = "root"; 
	$passwordDB = "";
		if ( $_GET['page']){
			$connect = mysql_connect($host, $userDB, $passwordDB);
			$DB = mysql_select_db("Polyglot");
			if (!$connect || !$DB) mysql_error();
			$login = $_COOKIE['login'];
			$page = $_GET['page'];
			$queryUsers = mysql_query("SELECT * FROM `Users` WHERE `login`='$login'");
			$queryWord = mysql_query("SELECT * FROM `Words` WHERE `idTopics`= $page");
			$queryTopic = mysql_query("SELECT * FROM `Topics` WHERE `id`=$page");			
			$dataUser = mysql_fetch_assoc($queryUsers);
			mysql_close();

			$topic = mysql_fetch_assoc($queryTopic);
			?> <h5><? echo $topic['topic'] ?> </h5><br>
			<div class="divider"></div><br>
			<table class="striped">
        		<thead>
         			<tr>
              		<th>Слово</th>
              		<th>Перевод</th>
              		<th>Использование</th>
          			</tr>
        		</thead>
        		<tbody>

			 <?	
			 if ($dataUser['isAdmin'] == 'false'){
			 	while ($arr = mysql_fetch_assoc($queryWord)){?>
			<tr>
            	<td><? echo $arr['word'] ?></td>
            	<td><? echo $arr['translate'] ?></td>
            	<td><? echo $arr['sentence'] ?></td>
          	</tr>

			<? }; ?>
			 </div>
			</div>
			</div>
			</div>
			<?}
			else if ($dataUser['isAdmin'] == 'true'){
				while ($arr = mysql_fetch_assoc($queryWord)){?>
					<tr>
		            	<td>
		            		<div class="input-field">
            					<input value="<? echo $arr['word'] ?>" id="first_name2" type="text">       			
            				</div>
            			</td>
		            	<td>
		            		<div class="input-field">
            					<input value="<? echo $arr['translate'] ?>" id="first_name2" type="text">       			
            				</div>
            			</td>
		            	<td>
		            		<div class="input-field">
            					<input value="<? echo $arr['sentence'] ?>" id="first_name2" type="text">       			
            				</div>
            			</td>
          			</tr>

			<?	} ?>
        	</tbody>
      	</table><br>
      	 </div>
		</div>
		</div>
		</div>
		<? } ?>
		<?}
		else { ?>
			<a style="font-size: 28px; color: black; font-weight: 399;">Словарь</a> 
			<div class="divider"></div><br>
			<ul class="collapsible" id="selection" style="font-size: 23px; width: 100%;margin: auto; font-weight: 399;">						
			</ul>
</div>
</div>
</div>
</div>
	    <script type="text/javascript">
	    	$(document).ready(function(){
	    		$(".button-collapse").sideNav();
	    	
				$.ajax({
					type: 'POST',
					url: 'API.php',
					dataType: 'text',
					data: {'getSection': true},
					success: function(data){
						data = JSON.parse(data);
						for (var i = 0; i < data.length; i++){
							var ul = document.getElementById('selection'),
								li = document.createElement('li'),
								divHeader = document.createElement('div'),
								icon = document.createElement('i'),
								a = document.createElement('a'),
								divBody = document.createElement('div');

							icon.className = "material-icons";
							icon.innerHTML = data[i].icon;
							a.innerHTML = data[i].name;
							a.style.color = "black";
							a.style.fontSize = "20px";
							divHeader.className = "collapsible-header";
							divBody.className = "collapsible-body";
							divBody.style.paddingLeft = "60px";
							divBody.style.paddingRight = "60px";
							li.onclick = info(divBody, data[i].id);

							divHeader.appendChild(icon);
							divHeader.appendChild(a);						
							li.appendChild(divHeader);
							li.appendChild(divBody);

							ul.appendChild(li);
						}
					}
				});		
			});

			function info(body, id){
				$.ajax({
					type: 'POST',
					url: 'API.php',
					dataType: 'text',
					data: {'getTopic': true, 'id':id},
					success: function(data){
						data = JSON.parse(data);
						console.log(data);
						for (var i = 0; i < data.length; i++){
							var info = document.createElement('a'),
								icon = document.createElement('i'),
								br = document.createElement('div');
							br.className = "divider";
							info.innerHTML = data[i].topic;
							info.href = data[i].link;
							info.style.color = "black";
							info.style.fontSize = "20px";
							icon.className = "material-icons";
							icon.innerHTML = "chevron_right";
							icon.style.fontSize = "13px";
							body.appendChild(icon);
							body.appendChild(info);
							body.appendChild(br);					
						}
					}
				});
			}
	    </script>
<?	} ?>
</body>
</html>