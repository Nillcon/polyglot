<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Polyglot</title>
	<!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <script type="text/javascript" src="js/lib.js"></script>
</head>
<body style="background-color: #f5f5f5">
<nav>
	<div class="nav-wrapper" style="background: #8A0829">
		<div class="container">
			<a href="index.php" class="brand-logo">Polyglot</a>
			<ul class="right hide-on-med-and-down">
				<li><a href="index.php">Главная</a></li>
				<li><a href="authorization.php">Войти</a></li>
			</ul>
		</div>
	</div>
</nav> 
	<br><br>

	<div class="row">
		<form action="CheckIn.php" method="POST">		
			<div class="col s6 offset-s3">
				<div class="card white" style="color: #424242;">
					<div class="style" style="padding: 20px">
						<div class="row">
							<div class="col s4 offset-s2">
								<h4 style="font-weight: 300; font-size: 35px">Регистрация</h4>
							</div><br>
						</div>
						<div class="row">
			        		<div class="input-field col s4 offset-s2">
			        			<i class="material-icons prefix">account_circle</i>
			          			<input id="first_name" type="text" class="validate" required>
			          			<label for="first_name" data-error="" id="labelName">Имя</label>
			        		</div>
			        		<div class="input-field col s4">
				          		<input id="last_name" type="text" class="validate" required>
				          		<label for="last_name" data-error="Не введена фамилия">Фамилия</label>
			        		</div>
			      		</div>

			      		<div class="row">
			      			<div class="input-field col s8 offset-s2">
			      				<i class="material-icons prefix">bug_report</i>
			      				<input id="login" type="text" class="validate" onblur="checkLog()" required>
			      				<label for="login" data-error="" id="log">Логин</label>
			      			</div>
			      		</div>

			      		<div class="row">
			        		<div class="input-field col s4 offset-s2" >
			        			<i class="material-icons prefix">vpn_key</i>
			          			<input id="password" type="password" class="validate" name="password" required>
			          			<label for="password" data-error="Введите пароль!">Пароль</label>
			        		</div>
			        		<div class="input-field col s4">
				          		<input id="re-password" type="password" class="validate" onblur="checkPass()" required>
				          		<label for="re-password" data-error="">Повторите пароль</label>
			        		</div>
			      		</div>

			      		<div class="row">
			      			<div class="input-field col s8 offset-s2">
			      				<i class="material-icons prefix">email</i>
			      				<input id="mail" type="email" class="validate" name="mail" required>
			      				<label for="mail" data-error="Не верно указана почта!" id="mail">Email</label>
			      			</div>
			      		</div>
			      		
			      		<div class="row">
			      			<div class="input-field col s8 offset-s2">
			      				<i class="material-icons prefix">phone</i>
			      				<input id="phone" type="text" class="validate" name="phone" required>
			      				<label for="phone">Номер телефона</label>
			      			</div>
			      		</div>

			      		<div class="row">
			      			<div class="input-field col 3 offset-s2">
			      				<h6 id="test" style="color: green"></h6>			      				
			      				<a class="waves-effect waves-light btn" style="background-color: #8A0829" onclick="reg()">Зарегистрироваться</a>
			      				<a href="http://localhost/Polyglot.ua/authorization.php" style="padding-left: 15px; color: #8A0829" >У меня уже есть аккаунт</a>
			      			</div>
			      		</div>			      		
					</div>
		      	</div>
			</div>
		</form>
	</div>

	<div class="footer-copyright">
		<br>
        <div class="center-align">
        	<h6>© by Nilcon</h6>
        </div>
    </div>

	<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="js/materialize.min.js"></script>
	<script type="text/javascript" src="js/jquery.maskedinput.min.js"></script>
	<script>
		jQuery(function($){
			$("#phone").mask("+38(999) 999-99-99");
		});

		function checkLog(){
			var log = document.getElementById("login").value;
			$.ajax({
				type: "POST",
				url: "API.php",
				data:{'checkLog': true, 'log': log},
				dataType: 'text',
				success: function (data) {
					if (data == 'true') document.getElementById('login').className = 'validate invalid';		
					else document.getElementById('login').className = 'validate valid';
				}
			});
		}

		function checkPass(){
			var pass = document.getElementById("password").value,
				re_pass = document.getElementById("re-password").value;
			if (pass != re_pass) document.getElementById("re-password").className = 'validate invalid';
			else document.getElementById("re-password").className = 'validate valid';			
		}

		function reg(){
			checkLog();
			var pass 		= document.getElementById("password").value,
				re_pass 	= document.getElementById("re-password").value,
				phone 		= document.getElementById("phone").value,
				log 		= document.getElementById("login").value,
				name 		= document.getElementById("first_name").value,
				surename  	= document.getElementById("last_name").value,
				mail 		= document.getElementById("mail").value;

			if (pass == re_pass && pass && phone && log &&  name && surename && mail){				
				$.ajax({
					type: "POST",
					url: "API.php",
					data: {'Reg_check': true, 'login': log, 'password': pass, 'phone': phone, 'mail': mail, 'first_name': name, 'last_name': surename },
					dataType: 'html',
					success: function(data){
						document.location = "authorization.php";
					}
				});
			}
		}
   	</script>	
</body>
</html>