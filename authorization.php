<!DOCTYPE html>
<html>
<head>
	<title>Polyglot</title>
	<meta charset="utf-8">
	<!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script type="text/javascript" src="js/lib.js"></script>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="background-color: #f5f5f5" id="body">
<nav>
<div class="nav-wrapper" style="background: #8A0829">
	<div class="container">
		<a href="index.php" class="brand-logo">Polyglot</a>
		<ul class="right hide-on-med-and-down">
			<li><a href="index.php">Главная</a></li>
			<li><a href="authorization.php">Войти</a></li>
		</ul>
	</div>
</div>
</nav> 
<br><br>

	<div class="row">
		<form action="authorization.php" method="POST">
			<div class="col s4 offset-s4">
				<div class="card white" >
					<div class="style" style="padding: 15px; color: #424242;">
						<div class="row">
							<div class="col s6 offset-s3">
								<h4 style="font-weight: 300; font-size: 35px"> Авторизация </h4>
							</div>
						</div>	
						<div class="row">
				      		<div class="input-field col s8 offset-s2">
			      				<i class="material-icons prefix">bug_report</i>
			      				<input id="login" type="text" class="validate" required>
			      				<label for="login">Логин</label>
				      		</div>
				      	</div>

			      		<div class="row">
			        		<div class="input-field col s8 offset-s2" >
		        				<i class="material-icons prefix">vpn_key</i>
		          				<input id="password" type="password" class="validate" name="password" required>
		          				<label for="password">Пароль</label>
			        		</div>
						</div>

						<div class="row">
			      			<div class="input-field col s7 offset-s3">
			      				<p id="Warning" style="display: none"></p>
			      				<a class="waves-effect waves-light btn" style="background-color: #8A0829" onclick="authorization()">Войти</a>
			      			</div>
				      	</div>
				    </div>	
				</div>					
			</div>
		</form>	
	</div>

	
	<div class="footer-copyright">
		<br>
        <div class="center-align">
        	<h6>© by Nilcon</h6>
        </div>
    </div>

		<!--Import jQuery before materialize.js-->
	    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	    <script type="text/javascript" src="js/materialize.min.js"></script>
	    <script type="text/javascript">
	    	$(document).ready(function(){
	    		$.ajax({
	    			type: 'POST',
	    			url: 'API.php',
	    			data: {'checkAuth': true},
	    			dataType: 'text',
	    			success: function(data){
	    				if (data == true) document.location = 'index.php';
	    				else if (data == false) console.log('Запрещено');
	    			}
	    		})
	    		document.onkeypress = function (event){
	    			if (event.charCode == 13) authorization() ;
	    		}
	    	});

	    	function authorization(){
	    		var login 		= document.getElementById('login').value,
	    			password 	= document.getElementById('password').value;
	    		
	    		$.ajax({
				type: "POST",
				url: "API.php",
				data:{'authorization': true, 'login': login, 'password': password},
				dataType: 'text',
				success: function (data) {
					if (data == 'false') {
						document.getElementById('Warning').style = "display: block; color: red";
						document.getElementById('Warning').innerHTML = "Неверно указан логин или пароль!";
					}
					else document.location = 'index.php';
				}
			});
	    	}
	    </script>
</body>
</html>