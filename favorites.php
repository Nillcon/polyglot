<!DOCTYPE html>
<html>
<head>
    <title>Polyglot</title>
    <meta charset="utf-8">
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script type="text/javascript" src="js/lib.js"></script>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <style>
        .userChoice:hover{
            cursor: pointer;
            background: rgba(0, 0, 0, .1);
            background-color: #D3D3D3;
        }
    </style>

</head>
<body style="background-color: #f5f5f5" id="body">
<nav>
<div class="nav-wrapper" style="background: #8A0829">
    <div class="container">
        <a href="index.php" class="brand-logo">Polyglot</a>
        <ul class="right hide-on-med-and-down">
            <li><a href="index.php">Главная</a></li>
            <li><a href="statistics.php" >Статистика</a></li>
            <li><a href="words.php" >Словарь</a></li>
            <li><a href="authorization.php">Войти</a></li>
        </ul>
    </div>
</div>
</nav> 
<br><br>

<? 
    //Данные о БД
    $host = "localhost";
    $userDB = "root";
    $passwordDB = "";
    $DB = "Polyglot";

    if ($_COOKIE['id'] && $_COOKIE['login'] && $_COOKIE['password']) {
        $id = $_COOKIE['id'];
        $login = $_COOKIE['login'];
        $password = $_COOKIE['password'];
        mysql_connect($host, $userDB, $passwordDB);
        mysql_select_db($DB);

        $queryUser = mysql_query(" SELECT * FROM `Users` WHERE `login`='$login' ");
        $user = mysql_fetch_assoc($queryUser);
        mysql_close();

        if (md5($user['password']) == $_COOKIE['password']){

            if ($user['isAdmin'] == 'false') {?>
                <div class="row">
                <div class="col s10 offset-s1">
                <div class="card grey light-blue lighten-5">
                <div class="style" style="padding: 20px" id="container">
                <a class="btn-floating waves-effect waves-light" 
                    href="index.php"
                    style=" margin-bottom:10px;
                            background-color:#8A0829;">
                    <i class="material-icons">arrow_back</i>
                </a>
                <a style="font-size: 28px; color: black; font-weight: 399; padding-left: 10px">Мои слова</a><br><br>
                    
                <table class="striped centered">
                    <thead>
                        <tr>
                            <th>№</th>
                            <th>Слово</th>
                            <th>Перевод</th>
                            <th>Пример</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(favorite, i) in favorite">
                            <td>{{ i+1 }}</td>
                            <td>{{ favorite.word }}</td>
                            <td>{{ favorite.translate }}</td>
                            <td>{{ favorite.sentence }}</td>
                        </tr>
                    </tbody>
                </table>
                <script> 
                    $.ajax({
                        type: 'POST',
                        url: 'API.php',
                        dataType: 'text',
                        data: {'getFavorite': true},
                        success: function(data) {
                            var app = new Vue({
                                el: '#container',
                                data: {
                                    favorite: JSON.parse(data)
                                }
                            });
                        }
                    })                   
                </script>

                </div>
                </div>
                </div>
                </div>

            <?} 
            else {
                if ($user['isAdmin'] == 'true') {
                    if ($_GET['favoriteList']) {?>
                        <a class="btn-floating waves-effect waves-light" 
                            href="index.php"
                            style="margin-left:20px;
                                    margin-bottom:10px;
                                    background-color:#8A0829">
                            <i class="material-icons">arrow_back</i>
                        </a>
                        <a style="font-size: 28px; color: black; font-weight: 399; padding: 20px;">Слова студентов</a><hr>
                        <table id="fav" class="striped centered">
                            <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Слово</th>
                                    <th>Перевод</th>
                                    <th>Пример</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(favorite, i) in favWord">
                                    <td>{{ i+1 }}</td>
                                    <td>{{ favorite.word }}</td>
                                    <td>{{ favorite.translate }}</td>
                                    <td>{{ favorite.sentence }}</td>
                                </tr>
                            </tbody>
                        </table>

                        <script>
                            var idUser = <? echo $_GET['favoriteList'] ?>;
                            $.ajax({
                                type: 'POST',
                                url: 'API.php',
                                dataType: 'text',
                                data: {'getFavorite': true, 'idUser': idUser},
                                success: function (data) {
                                    var app = new Vue({
                                        el: '#fav',
                                        data: {
                                            favWord: JSON.parse(data)
                                        }
                                    });
                                }
                            })
                        </script>
                    
                 <? }
                    else { ?>                        
                        <a class="btn-floating waves-effect waves-light" 
                            href="index.php"
                            style="margin-left:20px;
                                    margin-bottom:10px;
                                    background-color:#8A0829">
                            <i class="material-icons">arrow_back</i>
                        </a>
                        <a style="font-size: 28px; color: black; font-weight: 399; padding: 20px;">Слова студентов</a><hr>
                        <table id="fav" class="centered">
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>Имя</th>
                                <th>Фамилия</th>
                                <th>Mail@</th>
                                <th>Логин</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr v-for="user in arrUsers" class="userChoice" v-on:click="document.location += '?favoriteList=' + user.id">
                                    <td>{{ user.id }}</td>
                                    <td>{{ user.name }}</td>
                                    <td>{{ user.surename }}</td>
                                    <td>{{ user.mail }}</td>
                                    <td>{{ user.login }}</td>
                                </tr>
                            </tbody>
                        </table>
                        <script>
                            $.ajax({
                                type: 'POST',
                                url: 'API.php',
                                dataType: 'text',
                                data: {'getUsers': true},
                                success: function(data) {
                                    var app = new Vue({
                                        el: '#fav',
                                        data: {
                                            arrUsers: JSON.parse(data)
                                        }
                                    });
                                }
                            })
                        </script>
                 <? }
                }
                else {?>
                    <script> document.location = 'index.php'; </script> 
              <?}
            }          
        } 
        else {?> 
            <script> document.location = 'index.php'; </script> 
        <?}
    }
?>
</div>
</div>
</div>
</div>
</body>
</html>