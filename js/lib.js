function shuffle(array) {
	var buff = [], length = array.length;
	for (var i = 0; i < length; i++) {
		var numb = rnd(0, array.length);
		buff.push(array[numb]);
		array.splice(numb, 1);
	}
	return buff;
}

function rnd(min, max) {
	return Math.floor(Math.random() * (max - min) + min);
}

function exit() {
	$.ajax({
		type: "POST",
		url: "API.php",
		dataType: "text",
		data: {"exit": true},
		success: function(data) {
			document.location = "index.php"
		}
	})
}