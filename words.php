<!DOCTYPE html>
<html>
<head>
	<title>Polyglot</title>
	<meta charset="utf-8">
	<!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script type="text/javascript" src="js/lib.js"></script>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="background-color: #f5f5f5">
<nav>
	<div class="nav-wrapper" style="background: #8A0829">
		<div class="container">
			<a href="index.php" class="brand-logo">Polyglot</a>
			<ul class="right hide-on-med-and-down">
				<li><a href="index.php">Главная</a></li>
				<li><a href="statistics.php" >Статистика</a></li>
				<li><a href="words.php" >Словарь</a></li>
				<li><a onclick="exit()">Выйти</a></li>
			</ul>
		</div>
	</div>
</nav> 
	<br><br>

  <!-- Модульное окно добавления полей -->
  <div id="modal1" class="modal">
    <div class="modal-content">
      <h5>Сколько строк вы хотите добавить?</h5>
      <input type="text" id="addLines">
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Отменить</a>
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat" onclick="addLines()">Добавить</a>
    </div>
  </div>

  <div id="modal2" class="modal">
    <div class="modal-content">
      <h5>Добавить тему</h5>
      <div class="input-field">
      	<input type="text" id="addTopic">
      	<label for="addTopic">Название</label>
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Отменить</a>
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat" id="newTopic">Добавить</a>
    </div>
  </div>

	<?php

	$host = "localhost";
	$userDB = "root"; 
	$passwordDB = "";
	$DB = "Polyglot";

		if ( $_GET['topic']){
			$connect = mysql_connect($host, $userDB, $passwordDB);
			$DB = mysql_select_db($DB);
			if (!$connect || !$DB) mysql_error();

			$login = $_COOKIE['login'];
			$topic = $_GET['topic'];

			$queryUsers = mysql_query("SELECT * FROM `Users` WHERE `login`='$login'");
			$queryWord = mysql_query("SELECT * FROM `Words` WHERE `idTopics`= $topic");
			$queryTopic = mysql_query("SELECT * FROM `Topics` WHERE `id`=$topic");			
			$dataUser = mysql_fetch_assoc($queryUsers);
			mysql_close();

			$topic = mysql_fetch_assoc($queryTopic);
			?> 
			<div class="row">
			<div class="col s10 offset-s1">
			<div class="card grey light-blue lighten-5">
			<div class="style" style="padding: 20px" id="container">
			<a class="btn-floating waves-effect waves-light" 
				href="words.php"
				style="	margin-bottom:10px;
						margin-right: 10px;
						background-color:#8A0829">
				<i class="material-icons">arrow_back</i>
			</a>
			<a style="font-size: 28px; color: black; font-weight: 399;"><? echo $topic['topic'] ?> </a><br>

			<div class="divider"></div>
			<div id="app">

			 <?	
			if ($dataUser['isAdmin'] == 'false'){
				$number = 1;
				?>					
				<table class="striped centered" id="table">
        		<thead>
         			<tr id="tableHeader">
         			<th>№</th>
              		<th>Слово</th>
              		<th>Перевод</th>
              		<th>Использование</th>
          			</tr>
        		</thead>
        		<tbody>
				<?
				while ($arr = mysql_fetch_assoc($queryWord)){ ?>
					<tr>
						<td><? echo $number++ ?> </td>
		            	<td><? echo $arr['word'] ?></td>
		            	<td><? echo $arr['translate'] ?></td>
		            	<td><? echo $arr['sentence'] ?></td>
		          	</tr>
			<? }; ?>
				</tbody>
				</table>
			<?
			}
			else if ($dataUser['isAdmin'] == 'true'){
				?> 
				<div class="fixed-action-btn">
		 			<a class="btn-floating btn-large" style="background-color: #8A0829">
		    			<i class="large material-icons">mode_edit</i>
  					</a>
			  		<ul>
					    <li><a class="btn-floating red" onclick="save();"><i class="material-icons">save</i> </a></li>
					    <li><a class="btn-floating yellow darken-1 btn modal-trigger" href="#modal1"><i class="material-icons">add</i></a></li>
					    <li><a class="btn-floating green"><i class="material-icons">publish</i></a></li>
					    <li><a class="btn-floating blue"><i class="material-icons" onclick="delTopic()">delete</i></a></li>
			  		</ul>
				</div>

				<table class="striped centered" id="table" style="padding-bottom: 30px">
        		<thead>
         			<tr id="tableHeader">
         			<th>№</th>
              		<th>Слово</th>
              		<th>Перевод</th>
              		<th>Использование</th>
              		<th></th>
          			</tr>
        		</thead>
        		<tbody>	
				<tr v-for="(words, i) in word" v-show="words.word">
					<td>{{ i + 1 }}</td>
	            	<td><input type="text" v-model="words.word"></td>
	            	<td><input type="text" v-model="words.translate"></td>
	            	<td><input type="text" v-model="words.sentence"></td>
	            	<td><a href="#" style="color: black" v-on:click="del(i)"><i class="large material-icons" style="font-size: 20px">clear</i></a></td>
	          	</tr>
		        </tbody>
				</table>
				
				<script>						
					var answer,
						topic = <? echo $_GET['topic'] ?>;
				
					$.ajax({
						type: 'POST',
						url: 'API.php',
						dataType: 'text',
						data: {'getWord': true, 'topic': topic},
						success: function(data){
							answer = JSON.parse(data);
							var app = new Vue({
								el: '#app',
								data:{
									word: answer
								}									
							});
						}
					});	
				</script>
					
				<?
			};
		}
		else if ($_GET['сourse'] && $_GET['spec']){?> 
		<div class="row">
		<div class="col s8 offset-s2">
		<div class="card grey light-blue lighten-5">
			<div class="style" style="padding: 20px" id="container">
			<a class="btn-floating waves-effect waves-light" 
				href="words.php"
				style="	margin-bottom:10px;
						margin-right: 10px;
						background-color:#8A0829">
				<i class="material-icons">arrow_back</i>
			</a>
			<a style="font-size: 28px; color: black; font-weight: 399;" id="add">Навигация по темам</a><br><br>					
			<a style="font-size: 18px; color: black; font-weight: 399;"> 
				<span style="font-weight: 400;">Курс: </span><? echo $_GET['сourse']; ?> 
				<span style="font-weight: 400;">Cпециальность: </span><? echo $_GET['name']; ?></a>						
			<div class="divider"></div><br>
			<ul class="collapsible" data-collapsible="accordion" id="ulSpec">				    
		  	</ul>
			<script>	
				var spec = '<? echo $_GET['spec'] ?>';
				var course = '<? echo $_GET['сourse'] ?>';
				var dat = [];				

				$.ajax({
					type: 'POST',
					url: 'API.php',
					dataType: 'text',
					data: {'SelectTopic': true, 'spec': spec, 'course': course},
					success: function(data){
						data = JSON.parse(data);
						dat = data;
						for (var i = 0; i < data.length; i++){
							var ul = document.querySelector('#ulSpec');									
							var li = document.createElement('li'),
								header = document.createElement('div'),
								a = document.createElement('a'),
								icon = document.createElement('i');

							a.style = "color: black; font-size: 18px";
							icon.className = "material-icons";
							icon.innerHTML = "chevron_right";
							header.className = "collapsible-header";
							a.innerHTML = data[i].topic;
							a.href = "/Polyglot.ua/words.php?topic=" + data[i].id;

							header.appendChild(icon);
							header.appendChild(a);
							li.appendChild(header);
							ul.appendChild(li);
						}
					}
				})	
			</script>
		<? 
			mysql_connect($host, $userDB, $passwordDB);
			mysql_select_db($DB);
			$login = $_COOKIE['login'];
			$queryUsers = mysql_query("SELECT * FROM `Users` WHERE `login`='$login'");
			mysql_close();
			$dataUser = mysql_fetch_assoc($queryUsers);
			if ($dataUser['isAdmin'] == 'true') {?>
				<script>
					$('#add').after('<a href="#modal2" style="background-color:#8A0829" class="right btn modal-trigger">Добавить тему</a>');
					$('#newTopic').on('click', function() {
						var topic = $('#addTopic').val();
						$.ajax({
							type: 'POST',
							url: 'API.php',
							dataType: 'text',
							data: {'addTopic': true, 'spec': spec, 'course': course, 'topic': topic},
							success: function(data) {
								location.reload()
							}
						})
					})	
				</script>
		<?	}
		}
		else {?>
		<div class="row">
		<div class="col s10 offset-s1">
		<div class="card grey light-blue lighten-5">
		<div class="style" style="padding: 20px" id="container">
			<a class="btn-floating waves-effect waves-light" 
				href="index.php"
				style="	margin-bottom:10px;
						margin-right: 10px;
						background-color:#8A0829">
				<i class="material-icons">arrow_back</i>
			</a>
			<a style="font-size: 28px; color: black; font-weight: 399;">Навигация по темам</a>
			<div class="divider"></div><br>					
			<ul class="collapsible popout" data-collapsible="accordion" id="ulSelect">					    
		  	</ul>			
			<script type="text/javascript">

				$(document).ready(function(){
				    $('.collapsible').collapsible();
				});       
				
				$.ajax({
					type: 'POST',
					url: 'API.php',
					dataType: 'text',
					data: {'getSection': true},
					success: function(data){
						arrSpec = JSON.parse(data);
						arrCourse = ['I  Первый','II Второй','III Третий','IV Четвертый'];

						for (var i = 0; i < arrSpec.length; i++){
							var ul = document.querySelector('#ulSelect');
							var li = document.createElement('li'),
							header = document.createElement('div'),
							body = document.createElement('div'),
							logo = document.createElement('i'),
							name = document.createElement('span');

							header.className = "collapsible-header";
							logo.className = "material-icons";
							logo.innerHTML = arrSpec[i].icon;
							name.innerHTML = arrSpec[i].name;
							name.style="color: black; font-size: 20px; font-weight: 399;"
							header.appendChild(logo);
							header.appendChild(name);
							
							body.className = "collapsible-body";
							body.style = "font-size: 16px";
							for (var j = 0; j < arrCourse.length; j++){
								var a = document.createElement('a');
								a.innerHTML = arrCourse[j];
								a.style = "color: black;";
								a.href = document.location.href + '?сourse=' + (1+j) + '&spec=' + arrSpec[i].id + '&name=' + arrSpec[i].name;
								body.appendChild(a);
								body.appendChild(document.createElement('br'));
							}									

							li.appendChild(header);
							li.appendChild(body);
							ul.appendChild(li);
						}
					}
				}) 						
			</script>
			 <? } ?>
</div>
</div>
</div>
</div>
</div>
<div class="footer-copyright">
	<br>
    <div class="center-align">
    	<h6>© by Nilcon</h6>
    </div>
</div>

<script type="text/javascript">
	var topic;
	$(document).ready(function(){
		$('.modal').modal();
	});

	function save() {
		var saveWord = JSON.stringify(answer);
		console.log(saveWord);
		$.ajax({
			type: 'POST',
			url: 'API.php',
			dataType: 'text',
			data: {'save': true, 'arrayWord': saveWord},
			success: function(data){
				Materialize.toast('Изменения сохранены', 3000, 'rounded');
				console.log(data);
			}
		})
	};

	function add(){
		obj = {
			id : 'null',
			word: ' ',
			translate: '',
			sentence: '',
			idTopics: topic
		}
		answer.push(obj);
	}

	function del(id){
		answer[id].word = '';
		Materialize.toast('Запись удалена', 3000, 'rounded');
	}

	function addLines(){
		var lines = document.querySelector('#addLines');
		if (!isNaN(lines)) add();
		else if (isNaN(lines)){
			for (var i = 0; i < +lines.value; i++){
				add();
			} 
		}
	}

	function delTopic(){
		var id = Number(<? echo $_GET['topic'] ?>);
		$.ajax({
			type: 'POST',
			url: 'API.php',
			dataType: 'text',
			data: {'delTopic': true, 'id': id},
			success: function() {
				document.location = "words.php"
			}
		})
	}			
</script>
</body>
</html>