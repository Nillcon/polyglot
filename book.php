<!DOCTYPE html>
<html>
<head>
    <title>Polyglot</title>
    <meta charset="utf-8">
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script type="text/javascript" src="js/lib.js"></script>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="background-color: #f5f5f5">
<nav>
<div class="nav-wrapper" style="background: #8A0829">
    <div class="container">
        <a href="index.php" class="brand-logo">Polyglot</a>
        <ul class="right hide-on-med-and-down">
            <li><a href="index.php">Главная</a></li>
            <li><a href="statistics.php" >Статистика</a></li>
            <li><a href="words.php" >Словарь</a></li>
            <li><a href="authorization.php">Войти</a></li>
        </ul>
    </div>
</div>
</nav> 

<div id="modal1" class="modal">
<div class="modal-content">
<span style="font-size: 30px">Добавить книгу</span>
<form>
    <div class="input-field">
      <input id="nameBook" type="text" class="validate">
      <label for="nameBook">Название</label>
    </div>
    <div class="input-field">
      <input id="linkBook" type="text" class="validate">
      <label for="linkBook">Ссылка</label>
    </div>
</form>
</div>
<div class="modal-footer">
<a href="#!" id="add" class="modal-action modal-close waves-effect waves-green btn-flat">Добавить</a>
<a href="#!" class="modal-action modal-close waves-effect waves-red btn-flat">Отменить</a>
</div>
</div>
<br><br>
<div class="row">
<div class="col s8 offset-s2">
<div class="card grey light-blue lighten-5">
<div class="style" style="padding: 20px" id="container">

<a class="btn-floating waves-effect waves-light" 
    href="index.php"
    style=" margin-bottom:10px;
            background-color:#8A0829;">
    <i class="material-icons">arrow_back</i>
</a>
<a style="font-size: 28px; color: black; font-weight: 399; padding-left: 10px" id="books">Учебники</a>
<div class="divider"></div>
<br>
<? 
//Данные о БД
$host = "localhost";
$userDB = "root";
$passwordDB = "";
$DB = "Polyglot";

if ($_COOKIE['id'] && $_COOKIE['login'] && $_COOKIE['password']) {
    $id = $_COOKIE['id'];
    $login = $_COOKIE['login'];
    $password = $_COOKIE['password'];
    mysql_connect($host, $userDB, $passwordDB);
    mysql_select_db($DB);

    $user = mysql_query("SELECT * FROM `Users` WHERE `login`='$login'");
    $user = mysql_fetch_assoc($user);

    if (md5($user['password']) == $password) {?>            
        <div class="card hoverable valign-wrapper" v-for="(book, i) in books">
            <div class="pad valign-wrapper" style="padding: 10px">
                <i class="material-icons" style="font-size: 70px">description</i>
                <a class="black-text" v-bind:href="book.link" style="font-size: 17px">{{ book.name }}</a>
            </div>
            <a class="material-icons right delBook" href="#" style="color: black; display: none;" v-on:click="delBook(book.id, i)">delete</a>
        </div>
        <script>
            var arrBooks = []; 
            $.ajax({
                type: 'POST',
                url: 'API.php',
                dataType: 'text',
                data: {'getBooks': true},
                success: function(data) {
                    arrBooks = JSON.parse(data);
                    var app = new Vue({
                        el: '#container',
                        data: {
                            books: arrBooks
                        }
                    })
                }
            })
        </script>
        <?
        if ($user['isAdmin'] == 'true') {?>
            
            <script>
                $(document).ready(function() {
                    $('.modal').modal();
                    $('#books').after('<a href="#modal1" style="background-color:#8A0829" class="right btn modal-trigger" id="trig">Добавить книгу</a>');
                    $('#add').on('click', function() {
                        var name = $('#nameBook').val();
                        var link = $('#linkBook').val();
                        arrBooks.push({'name': name, 'link': link, 'id': null});
                        $.ajax({
                            type: 'POST',
                            url: 'API.php',
                            dataType: 'text',
                            data: {'Books': true,'addBook': true,'name': name, 'link': link}
                        })
                    });
                    $('.delBook').show();
                });

                function delBook(id, i){
                    $.ajax({
                        type: 'POST',
                        url: 'API.php',
                        dataType: 'text',
                        data: {'Books': true, 'delBook': true, 'idBook': id},
                        success: function(data){
                            arrBooks.splice(i, 1);
                        }
                    })
                }
            </script>

      <?}?>
    <?} 
    else {?>
        <script> document.location = 'index.php'; </script>
    <?};
}
?>
</div>
</div>
</div>
</div>
</body>
</html>