-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 28 2018 г., 21:39
-- Версия сервера: 5.6.37
-- Версия PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `Polyglot`
--

-- --------------------------------------------------------

--
-- Структура таблицы `Section`
--

CREATE TABLE `Section` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `icon` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Section`
--

INSERT INTO `Section` (`id`, `name`, `icon`) VALUES
(1, 'Обслуговування компютерних систем і мереж', 'network_check'),
(2, 'Розробка програмного забезпечення', 'devices_other'),
(3, 'Компьютерна iнженерiя', 'memory'),
(4, 'Бухгалтерський облік', 'attach_money'),
(5, 'Економіка підприємства', 'business'),
(6, 'Інформаційна діяльність підприємства', 'info_outline'),
(7, 'Технологія обробки матеріалів', 'build');

-- --------------------------------------------------------

--
-- Структура таблицы `SelectedUserWords`
--

CREATE TABLE `SelectedUserWords` (
  `idUsers` int(11) NOT NULL,
  `idWords` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `Topics`
--

CREATE TABLE `Topics` (
  `id` int(11) NOT NULL,
  `idSpecialty` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `topic` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Topics`
--

INSERT INTO `Topics` (`id`, `idSpecialty`, `course`, `topic`, `link`) VALUES
(1, 2, 0, 'Бизнес', 'http://localhost/Polyglot.ua/words.php?page=1'),
(2, 3, 0, 'Компьютеры', 'http://localhost/Polyglot.ua/words.php?page=2'),
(3, 2, 0, 'Интернет', 'http://localhost/Polyglot.ua/words.php?page=3'),
(4, 2, 0, 'Поисковики', 'http://localhost/Polyglot.ua/words.php?page=4'),
(5, 3, 0, 'Материнская плата', 'http://localhost/Polyglot.ua/words.php?page=5'),
(6, 3, 0, 'Видеокарта', 'http://localhost/Polyglot.ua/words.php?page=6');

-- --------------------------------------------------------

--
-- Структура таблицы `Users`
--

CREATE TABLE `Users` (
  `id` int(11) NOT NULL,
  `login` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `surename` varchar(100) NOT NULL,
  `phone number` varchar(20) NOT NULL,
  `mail` varchar(30) NOT NULL,
  `isAdmin` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Users`
--

INSERT INTO `Users` (`id`, `login`, `password`, `name`, `surename`, `phone number`, `mail`, `isAdmin`) VALUES
(1, 'Nilcon', 'ed088edbcf0091943913d5438055b230', 'Вадим', 'Сидорчук', '+38(067) 698-38-77', 'Nilcon@mail.ru', 'false'),
(2, 'Dasya', '81dc9bdb52d04dc20036dbd8313ed055', 'Даша', 'Гордок', '+38(918) 395-89-13', 'Dasya@mail.ru', 'false');

-- --------------------------------------------------------

--
-- Структура таблицы `Words`
--

CREATE TABLE `Words` (
  `id` int(11) NOT NULL,
  `word` varchar(50) NOT NULL,
  `translate` varchar(300) NOT NULL,
  `sentence` varchar(300) NOT NULL,
  `idTopics` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Words`
--

INSERT INTO `Words` (`id`, `word`, `translate`, `sentence`, `idTopics`) VALUES
(1, 'Computer', 'Компьютер', 'All our customer orders are handled by computer.', 1),
(2, 'Neighbor', 'Сосед', 'I live peacefully with my neighbors', 2),
(3, 'Guest', 'Гость', 'I am waiting for some guests', 3),
(4, 'Chief', 'Начальник', 'She is the chief ', 4),
(5, 'Parents', 'Родители', 'My parents lived in the country', 5),
(6, 'Husband', 'Муж', 'Her husband is a businessman', 6),
(7, 'Hello', 'Привет', 'Hello', 1),
(8, 'Word', 'Слово', 'Word', 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `Section`
--
ALTER TABLE `Section`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `Topics`
--
ALTER TABLE `Topics`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `Words`
--
ALTER TABLE `Words`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `Section`
--
ALTER TABLE `Section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `Topics`
--
ALTER TABLE `Topics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `Users`
--
ALTER TABLE `Users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `Words`
--
ALTER TABLE `Words`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
