<!DOCTYPE html>
<html>
<head>
	<title>Polyglot</title>
	<meta charset="utf-8">
	<!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <script type="text/javascript" src="js/lib.js"></script>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="background-color: #f5f5f5">

	<?php 
		$host = "localhost";
		$userDB = "root"; 
		$passwordDB = "";

		if (isset($_COOKIE['login']) && $_COOKIE['password'] && $_COOKIE['id']){
			mysql_connect($host, $userDB, $passwordDB);
			mysql_select_db('Polyglot');

			$cookieLogin = $_COOKIE['login'];
			$cookiePass = $_COOKIE['password'];
			$query = mysql_query("SELECT * FROM `Users` WHERE `login`='$cookieLogin'");
			mysql_close();
			$arr = mysql_fetch_assoc($query);

			if ($arr){
				if (md5($arr['password']) == $cookiePass){
					if($arr['isAdmin'] == 'false') require('user.html');
					else if($arr['isAdmin'] == 'true') require('admin.html');
				}
			}
			else require('greeting.html');		
		}
		else require('greeting.html');
	?>

	<div class="footer-copyright">
		<br>
	    <div class="center-align">
	    	<h6>© by Nilcon</h6>
	    </div>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="js/materialize.min.js"></script>
	<script>
		$(document).ready(function(){
    		$('ul.tabs').tabs();    	
	    });  		
	</script>
</body>
</html>
